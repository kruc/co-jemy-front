import { Component } from '@angular/core';
import { ROUTER_DIRECTIVES } from "@angular/router";

import { SupplierService } from './shared/supplier.service';

import '../style/app.scss';

/*
 * App Component
 * Top Level Component
 */
@Component({
  selector: 'my-app', // <my-app></my-app>
  providers: [SupplierService],
  directives: [...ROUTER_DIRECTIVES],
  template: require('./app.component.html'),
  styles: [require('./app.component.scss')],
})
export class AppComponent {
  constructor() {
  }
}
