import {Component, OnInit} from '@angular/core';
import {SupplierService} from "../../shared/supplier.service";

@Component({
  selector: 'cj-suppliers',
  template: require('./suppliers.component.html'),
  styles: [require('./suppliers.component.scss')]
})
export class SuppliersComponent implements OnInit {

  constructor(private suppliersService: SupplierService) {
  }

  ngOnInit() {
    console.log('Hello Suppliers');
    this.suppliersService.loadSuppliers();
  }

  createOrder(item) {
    console.log("Creating new order for supplier: ", item.name);
  }
}

