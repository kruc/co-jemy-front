import { HomeComponent } from './home';
import { provideRouter, RouterConfig } from '@angular/router';
import { SuppliersRoutes } from "./components/suppliers/suppliers.routes";
import { SuppliersComponent } from "./components/suppliers/suppliers.component";


const routes: RouterConfig = [
  {path: "", component: HomeComponent},
  ...SuppliersRoutes
];


export const APP_ROUTER_PROVIDERS = [
  provideRouter(routes)
];
