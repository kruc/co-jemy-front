import {Injectable} from '@angular/core';
import {Supplier} from "../models/supplier.model";
import {Http, Headers} from "@angular/http";

@Injectable()
export class SupplierService {
  baseUrl = "http://private-7f7b0-cojemy.apiary-mock.com/api/suppliers";
  suppliers: Supplier[] = [
    new Supplier(1, 'Sushi Kushi', 10, 100),
    new Supplier(2, 'Na masę', 12, 150),
    new Supplier(3, 'Bar Teatralny', 10, 10)
  ];

  constructor(private http: Http) {
  }

  loadSuppliers() {}

  logError(err) {
    console.error('There was an error: ' + err);
  }
}
