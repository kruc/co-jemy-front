# Co-jemy Frontend app


## Requirements

To run this app you need a decent version of `node` and `npm`.
Ensure you're running Node `v4.x.x +` and NPM `v3.x.x +`


## Installation:

Go to the project directory:

    $ cd frontend

Install the dependencies (may take a while...):

    $ npm install

## Running

Start the dev server:

    $ npm start

It will start a local server using `webpack-dev-server` which will watch,
build (in-memory), and reload for you.

Go to [http://localhost:8080](http://localhost:8080) in your browser.


## Building project

To build production-ready package, just run: `npm run build` - this will compile all needed files and them all into
`/dist` directory. You can serve those files via Apache/Nginx/whatever


## Testing

#### 1. Unit Tests

* single run: `npm test`
* live mode (TDD style): `npm run test-watch`

#### 2. End-to-End Tests

* single run:
  * in a tab, *if not already running!*: `npm start`
  * in a new tab: `npm run webdriver-start`
  * in another new tab: `npm run e2e`
* interactive mode:
  * instead of the last command above, you can run: `npm run e2e-live`
  * when debugging or first writing test suites, you may find it helpful to try out Protractor commands without starting up the entire test suite. You can do this with the element explorer.
  * you can learn more about [Protractor Interactive Mode here](https://github.com/angular/protractor/blob/master/docs/debugging.md#testing-out-protractor-interactively)

## Documentation

You can generate api docs (using [TypeDoc](http://typedoc.io/)) for your code with the following:

    npm run docs

Have fun!
